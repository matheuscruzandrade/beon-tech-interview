﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class PassengersController(
    IService<Passenger> personService,
    IRepository<Flight> flightRepository,
    IMapper mapper)
    : SecureFlightBaseController(mapper)
{
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<PassengerDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var passengers = await personService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Passenger>, IReadOnlyList<PassengerDataTransferObject>>(passengers);
    }

    [HttpGet("/flights/{flightId:long}/passengers")]
    [ProducesResponseType(typeof(IEnumerable<PassengerDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> GetPassengersByFlight(long flightId)
    {
        var passengers = await personService.FilterAsync(p => p.Flights.Any(x => x.Id == flightId));
        return !passengers.Succeeded ?
            NotFound($"No passengers were found for the flight {flightId}") :
            MapResultToDataTransferObject<IReadOnlyList<Passenger>, IReadOnlyList<PassengerDataTransferObject>>(passengers);
    }

    [HttpPost("{fligthId}")]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Post([FromRoute] long fligthId, PassengerDataTransferObject passengerDto)
    {
        var flight = await flightRepository.GetByIdAsync(fligthId);

        if(flight is null)
        {
            return NotFound("Fligth not found");
        }

        var passenger = await personService.FindAsync(passengerDto.Id);

        if(passenger is null)
        {
            return NotFound("Pasenger not found");
        }

        var passengersInFlight = await personService.FilterAsync(p => p.Flights.Any(x => x.Id == fligthId));

        if(passengersInFlight.Result.Any(x => x.Id == passenger.Result.Id))
        {
            return NotFound("Pasenger in flight already");
        }

        flight.Passengers.Add(passenger);

        flightRepository.Update(flight);
        await flightRepository.SaveChangesAsync();

        return MapResultToDataTransferObject<Flight, FlightDataTransferObject>(flight);
    }
}